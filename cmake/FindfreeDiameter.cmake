# FindfreeDiameter -- Locate freeDiameter resources
#
# This module locates the include directory and the daemon program.
#
# Usage example:
#    find_package (freeDiameter 1.2.0 REQUIRED)
#
# Normally, this module exports:
#  - freeDiameter_FOUND
#  - freeDiameter_LIBRARIES
#  - freeDiameter_INCLUDE_DIRS
#  - freeDiameter_VERSION defaults to "UNKNOWN"
#  - freeDiameter_EXTENSION_DIR
#
# In addition, if the freeDiameterd daemon is found:
#  - freeDiameter_EXECUTABLE is set to the daemon path
#  - freeDiameter_VERSION    is set to the daemon version
#
# The module also provides a rule to define a new extension,
#  - add_freeDiameter_extension (EXTNAME [EXCLUDE_FROM_ALL] SOURCE...)
#
# Extensions install into ${freeDiameter_EXTENSION_DIR} for boh their
# LIBRARY and RUNTIME DESTINATION.
#
# From: Rick van Rein <rick@openfortress.nl>


include(FindPackageHandleStandardArgs)

find_library (freeDiameter_fdcore_LIBRARY fdcore)
find_library (freeDiameter_fdproto_LIBRARY fdproto)

find_path (freeDiameter_EXTENSION_DIR
    dict_nasreq.fdx
    PATH_SUFFIXES lib/freeDiameter
)

find_path (freeDiameter_INCLUDE_DIR
    libfdcore.h
    PATHS ${CMAKE_INCLUDE_PATH} ${CMAKE_SYSTEM_INCLUDE_PATH}
    # NO_DEFAULT_PATH
    PATH_SUFFIXES freeDiameter
)

string (REGEX REPLACE "[/\\]freeDiameter$" ""
	freeDiameter_INCLUDE_DIR
	"${freeDiameter_INCLUDE_DIR}")

find_program (freeDiameter_EXECUTABLE
    freeDiameterd)

set (freeDiameter_VERSION "UNKNOWN")
if (freeDiameter_EXECUTABLE)
    execute_process (COMMAND ${freeDiameter_EXECUTABLE} --version
        OUTPUT_VARIABLE freeDiameter_VERSION_OUTPUT)
    if ("${freeDiameter_VERSION_OUTPUT}" MATCHES "^.*version *([^ \n]+)")
        set (freeDiameter_VERSION "${CMAKE_MATCH_1}")
    endif ()
endif ()

find_package_handle_standard_args (freeDiameter
    REQUIRED_VARS freeDiameter_fdcore_LIBRARY
                  freeDiameter_fdproto_LIBRARY
                  freeDiameter_INCLUDE_DIR
                  freeDiameter_EXTENSION_DIR
    VERSION_VAR freeDiameter_VERSION
)

if (freeDiameter_FOUND)
    message (STATUS "Found freeDiameter version ${freeDiameter_VERSION}")
    set (freeDiameter_LIBRARIES
        ${freeDiameter_fdcore_LIBRARY}
        ${freeDiameter_fdproto_LIBRARY})
    set (freeDiameter_INCLUDE_DIRS
        ${freeDiameter_INCLUDE_DIR})
    add_library (freeDiameter::fdcore  UNKNOWN IMPORTED)
    add_library (freeDiameter::fdproto UNKNOWN IMPORTED)
    set_property (TARGET freeDiameter::fdcore
        PROPERTY IMPORTED_LOCATION "${freeDiameter_fdcore_LIBRARY}")
    set_property (TARGET freeDiameter::fdproto
        PROPERTY IMPORTED_LOCATION "${freeDiameter_fdproto_LIBRARY}")
    set_property (TARGET freeDiameter::fdcore
        PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${freeDiameter_INCLUDE_DIR}")
    set_property (TARGET freeDiameter::fdproto
        PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${freeDiameter_INCLUDE_DIR}")
endif ()

# add_freeDiameter_extension (EXTNAME [EXCLUDE_FROM_ALL] SOURCE...)
#
# Library naming is non-standard: no "libX" and extension ".fdx" instead of ".so"
#
# Extensions install into ${freeDiameter_EXTENSION_DIR} for boh their
# LIBRARY and RUNTIME DESTINATION:
#   install (...
#       LIBRARY DESTINATION ${freeDiameter_EXTENSION_DIR}
#       RUNTIME DESTINATION ${freeDiameter_EXTENSION_DIR}
#   )
#
macro (add_freeDiameter_extension EXTNAME)
    add_library ("${EXTNAME}" MODULE ${ARGN})
    set_target_properties ("${EXTNAME}" PROPERTIES PREFIX ""    )
    set_target_properties ("${EXTNAME}" PROPERTIES SUFFIX ".fdx")
endmacro (add_freeDiameter_extension)

